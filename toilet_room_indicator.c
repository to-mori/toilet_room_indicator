#include <xc.h>
#define _XTAL_FREQ 8000000
#pragma config BOREN = OFF
#pragma config FOSC = INTRC_NOCLKOUT
#pragma config FCMEN = OFF
#pragma config MCLRE = OFF
#pragma config WDTE = OFF
#pragma config LVP = OFF
#pragma config PWRTE = ON

#define timerH cf
#define timerL 2c
#define Close_Counter 200                              //close time counter
#define Flick_Off_Counter 10                            //LED flick OFFtime counter
#define Flick_On_Counter 20                             //LED flick ONtime counter

#define Close_Sensor  RB0                             //close sensor = RB0
#define RED_LED RC2                                   //RED LED = RC2
#define BLUE_LED RC3                                  //BLUE LED = RC3

int cc;                                               //close time counter
int fc;                                               //LED flick time counter


/*main setting*/
void main(void)
{
////input////
//RB0:close sensor

////output////
//RC2:RED LED
//RC3:BLUE LED

OSCCON=0b01110000;                                          //clock 8MHz
CM1CON0=0b00000000;                                         //comparator OFF
PORTA=0b00000000;                                           //portA output 0
PORTB=0b00000000;                                           //portB output 0
PORTC=0b00000000;                                           //portC output 0
TRISA=0b00000000;                                           //portA input -
TRISB=0b00000001;                                           //portB input RB0
TRISC=0b00000000;                                           //portC input -

ANSEL=0b00000000;                                           //nothing analog
ANSELH=0b00000000;                                          //nothing analog

WPUB=0b00000001;                                            //RB0 internal pull up
OPTION_REG=0b00000000;                                      //pull up enable

/*timer interrupt 1 setting*/
T1CON=0x30;                                                 //interval timer
TMR1H=0xtimerH;                                             //timer H
TMR1L=0xtimerL;                                             //timer L
TMR1IE=1;                                                   //interrupt ok
TMR1ON=1;                                                   //timer1 start

PEIE=1;                                                     //all interrupt ok
GIE=1;                                                      //all interrupt ok

/*default*/
cc=0;                                                       //close time counter = 0
fc=0;                                                       //LED flick time counter = 0


///////////////////////main loop//////////////////////////
while(1)                                                    //main loop
    {
    }
}
/////////////////////timer interrupt//////////////////////
void interrupt A(void)
{
    if(TMR1IF)
        {
        TMR1H=0xtimerH;                                     //timer H
        TMR1L=0xtimerL;                                     //timer L
        }

    if(Close_Sensor==0)                                     //close sensor ON
        {
        if(cc<Close_Counter)
            {
            RED_LED=1;                                      //RED LED ON
            BLUE_LED=0;                                     //BLUE LED OFF
            cc++;                                           //close time counter ++
            }
        else
            {
            if(fc<Flick_Off_Counter)                        //if flick time counter 0.5s
                {
                RED_LED=0;                                  //RED LED OFF
                BLUE_LED=0;                                 //BLUE LED OFF
                fc++;                                       //flick time counter ++
                }
            else
                {
                if(fc<Flick_On_Counter)                     //if flick time counter 1.0s
                    {
                    RED_LED=1;                              //RED LED ON
                    BLUE_LED=0;                             //BLUE LED OFF
                    fc++;                                   //flick time counter ++
                    }
                else
                    {
                    fc=0;                                   //flick time counter reset
                    }
                }
            }
        }
    else
        {
        RED_LED=0;                                          //RED LED OFF
        BLUE_LED=1;                                         //BLUE LED ON
        cc=0;                                               //close time counter reset
        fc=0;                                               //flick time counter reset
        }

TMR1IF=0;                                               //timer1 flag clear
}
